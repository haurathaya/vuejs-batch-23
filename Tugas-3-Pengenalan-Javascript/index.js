// soal 1 //

var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"

pertama1 = pertama.substring(0,4)
pertama2 = pertama.substring(11,19)
concper = pertama1.concat(pertama2)

kedua1 = kedua.substring(0,7)
kedua2 = kedua.substring(7,18)
concked = kedua1.concat(kedua2.toUpperCase())

console.log(concper.concat(concked))

// soal 2 //

var kataPertama = "10"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "6"

var num1 = Number(kataPertama)
var num2 = Number(kataKedua)
var num3 = Number(kataKetiga)
var num4 = Number(kataKeempat)

console.log((num1 + num4) + (num2 * num3))

// soal 3 //
var kalimat = 'wah javascript itu keren sekali'

var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.substring(15, 18)
var kataKeempat = kalimat.substring(19, 25)
var kataKelima = kalimat.substring(25, 33)

console.log('Kata Pertama: ' + kataPertama)
console.log('Kata Kedua: ' + kataKedua)
console.log('Kata Ketiga: ' + kataKetiga)
console.log('Kata Keempat: ' + kataKeempat)
console.log('Kata Kelima: ' + kataKelima)