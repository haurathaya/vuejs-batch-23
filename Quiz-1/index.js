<<<<<<< HEAD
=======
// 1. Judul : Function Penghitung Jumlah Kata //

//Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

function jumlah_kata(kalimat) {
	var teksArr = kalimat.split(" ");
	var totalHuruf = 0;

	for (var i = 0; i < teksArr.length; i++) {
			totalHuruf++
		}
	console.log(totalHuruf)
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"
var kalimat_3 = "Halo, perkenalkan aku Haura"


jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4

// 2. Judul : Function Penghasil Tanggal Hari Esok //

// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

function next_date(tanggal, bulan, tahun) {
	
	if (tanggal == 28 && bulan == 2) {
		tanggal = 1
		bulan++
	} else if (tanggal == 29 && bulan == 2) {
		tanggal = 1
		bulan++
	} else if (tanggal == 31 && bulan == 12) {
		tanggal = 1
		bulan = 1
		tahun++
	} else if (tanggal == 31 && (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10)) {
		tanggal = 1
		bulan++
	} else if (tanggal == 30 && (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11)) {
		tanggal = 1
		bulan++
	} else {
		tanggal++
	}

	switch(bulan) {
	case 1:   { console.log(tanggal, 'Januari', tahun); break; }
	case 2:   { console.log(tanggal, 'Februari', tahun); break; }
	case 3:   { console.log(tanggal, 'Maret', tahun); break; }
	case 4:   { console.log(tanggal, 'April', tahun); break; }
	case 5:   { console.log(tanggal, 'Mei', tahun); break; }
	case 6:   { console.log(tanggal, 'Juni', tahun); break; }
	case 7:   { console.log(tanggal, 'Juli', tahun); break; }
	case 8:   { console.log(tanggal, 'Agustus', tahun); break; }
	case 9:   { console.log(tanggal, 'September', tahun); break; }
	case 10:  { console.log(tanggal, 'Oktober', tahun); break; }
	case 11:  { console.log(tanggal, 'November', tahun); break; }
	case 12:  { console.log(tanggal, 'Desember', tahun); break; }
	}
}

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal, bulan, tahun) // output : 1 Maret 2020

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal, bulan, tahun) // output : 1 Maret 2020

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal, bulan, tahun) // output : 1 Januari 2021

var tanggal = 31
var bulan = 7
var tahun = 2019

next_date(tanggal, bulan, tahun) // output : 1 Agustus 2019
>>>>>>> upstream/master
