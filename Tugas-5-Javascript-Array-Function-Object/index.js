// soal 1 //

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var i = 1; i < daftarHewan.length; i++) {
	for (var j = 0; j < i; j++) {
		if (daftarHewan[i] < daftarHewan[j]) {
			var k = daftarHewan[i];
			daftarHewan[i] = daftarHewan[j];
			daftarHewan[j] = k;
		}
	}
}

for (var i = 0; i < daftarHewan.length; i++) {
	console.log(daftarHewan[i])
}
console.log("\n")

// soal 2 //

function introduce(par1) {
	return ("Nama saya " + par1.name + ", umur saya " + par1.age + ", alamat saya di " + par1.address + ", dan saya punya hobby yaitu " + par1.hobby)
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

console.log("\n")

// soal 3 //

function isVocal(v) {
	return ['a', 'i', 'u', 'e', 'o'].indexOf(v.toLowerCase()) !== -1;
}

function hitung_huruf_vokal(par1, par2) {
	var teksArr1 = par1.split("");
	var totalVokal1 = 0;
	for (var i = 0; i < teksArr1.length; i++) {
		if(isVocal(teksArr1[i]) == true ) {
			totalVokal1++
		}
	}
	return totalVokal1

	var teksArr2 = par2.split("");
	var totalVokal2 = 0;
	for (var j = 0; j < teksArr2.length; j++) {
		if(isVocal(teksArr2[j]) == true ) {
			totalVokal2++
		}
	}

	return totalVokal2
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2)

console.log("\n")

// soal 4 //

function hitung(int) {
	arr = [-2, 0, 2, 4, 6, 8]
	return(arr[int])
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8