// soal 1 //

var nilai = 95;

if ( nilai >= 85)
	console.log("Indeksnya A")
else if ( nilai >= 75 && nilai < 85 )
	console.log("Indeksnya B")
else if ( nilai >= 65 && nilai < 75 )
	console.log("Indeksnya C")
else if ( nilai >= 55 && nilai < 65 )
	console.log("Indeksnya D")
else
	console.log("Indeksnya E")

// soal 2 //

var tanggal = 24
var bulan = 6
var tahun = 2000

switch(bulan) {
  case 1:   { console.log(tanggal, 'Januari', tahun); break; }
  case 2:   { console.log(tanggal, 'Februari', tahun); break; }
  case 3:   { console.log(tanggal, 'Maret', tahun); break; }
  case 4:   { console.log(tanggal, 'April', tahun); break; }
  case 5:   { console.log(tanggal, 'Mei', tahun); break; }
  case 6:   { console.log(tanggal, 'Juni', tahun); break; }
  case 7:   { console.log(tanggal, 'Juli', tahun); break; }
  case 8:   { console.log(tanggal, 'Agustus', tahun); break; }
  case 9:   { console.log(tanggal, 'September', tahun); break; }
  case 10:  { console.log(tanggal, 'Oktober', tahun); break; }
  case 11:  { console.log(tanggal, 'November', tahun); break; }
  case 12:  { console.log(tanggal, 'Desember', tahun); break; }
  default:  { console.log('Masukkan Bulan Yang Tepat!'); }}

  // soal 3 //

var n = 7
var pagar = ''

for (var i = 0; i < n; i++) {
	for (var j = 0; j <= i; j++) {
		pagar += '# '
	}
	pagar += '\n'
}
console.log(pagar)

// soal 4 //

var m = 10

for (var i = 1; i <= m; i++) {
	switch(i) {
		case 1: {	console.log(i, ' - I Love Programming'); break; }
		case 2: {	console.log(i, ' - I Love Javascript'); break; }
		case 3: {	console.log(i, ' - I Love VueJS\n==='); break; }
		case 4: {	console.log(i, ' - I Love Programming'); break; }
		case 5: {	console.log(i, ' - I Love Javascript'); break; }
		case 6: {	console.log(i, ' - I Love VueJS\n==='); break; }
		case 7: {	console.log(i, ' - I Love Programming'); break; }
		case 8: {	console.log(i, ' - I Love Javascript'); break; }
		case 9: {	console.log(i, ' - I Love VueJS\n==='); break; }
		case 10: {	console.log(i, ' - I Love Programming'); break; }
		case 11: {	console.log(i, ' - I Love Javascript'); break; }
		case 12: {	console.log(i, ' - I Love VueJS\n==='); break; }
	}
}

// 1. Judul : Function Penghitung Jumlah Kata //

//Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

function jumlah_kata(kalimat) {
	var teksArr = kalimat.split(" ");
	var totalHuruf = 0;

	for (var i = 0; i < teksArr.length; i++) {
			totalHuruf++
		}
	console.log(totalHuruf)
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"
var kalimat_3 = "Halo, perkenalkan aku Haura"


jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4

// 2. Judul : Function Penghasil Tanggal Hari Esok //

// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

function next_date(tanggal, bulan, tahun) {
	if (tanggal == 28 && bulan == 2) {
		tanggal = 1
		bulan++
	} else if (tanggal == 29 && bulan == 2) {
		tanggal = 1
		bulan++
	} else if (tanggal == 31 && bulan == 12) {
		tanggal = 1
		bulan = 1
		tahun++
	} else {
		tanggal++
		bulan++
	}

	s
}

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021